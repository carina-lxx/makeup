package com.galvanize.makeup;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class MakeupController {
    MakeupService makeupService;

    public MakeupController(MakeupService makeupService) {
        this.makeupService = makeupService;
    }
    @GetMapping("/api/makeups")
    public ResponseEntity<ItemList> getItems(@RequestParam(required = false) String brand,
                                             @RequestParam(required = false) String category) {
        ItemList list;
        if(brand == null && category == null) {
            list =  makeupService.getItems();
         }
//        else if(category == null) {
//            list = makeupService.getItemsByBrand(brand);
//        } else if(brand == null) {
//            list = makeupService.getItemsByCategory(category);
//        }
        else {
            list =  makeupService.getItems(brand, category);
        }
        return list.isEmpty() ? ResponseEntity.noContent().build() :
                ResponseEntity.ok(list);
    }

    @PostMapping("/api/makeups")
    public Item addItem(@RequestBody Item newItem) {
        return makeupService.addItem(newItem);
    }

    @GetMapping("/api/makeups/{barcode}")
    public ResponseEntity<Item> getAuto(@PathVariable String barcode) {
        Item target = makeupService.getItem(barcode);
        if (target == null) {
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.ok(target);
        }
    }

    @PatchMapping("/api/makeups/{barcode}")
    public ResponseEntity<Item> updateItem(@PathVariable String barcode,
                           @RequestBody UpdateRequest update) {
        try{
            Item updated = makeupService.updateItem(barcode, update.getColor());
            updated.setColor(update.getColor());
            return ResponseEntity.ok(updated);
        } catch(ItemNotFoundException e) {
            return ResponseEntity.noContent().build();
        }

    }

    @DeleteMapping("/api/makeups/{barcode}")
    public ResponseEntity deleteAuto(@PathVariable String barcode) {
        try {
            makeupService.deleteItem(barcode);
            return ResponseEntity.accepted().build();
        } catch(ItemNotFoundException e) {
            return ResponseEntity.noContent().build();
        }

    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public void invalidMakeupExceptionHandler(InvalidMakeupException e) {
    }

}
