package com.galvanize.makeup;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class MakeupService {

    MakeupRepository makeupRepository;

    public MakeupService(MakeupRepository makeupRepository) {
        this.makeupRepository = makeupRepository;
    }

    public ItemList getItems() {
        return new ItemList(makeupRepository.findAll());
    }

    public ItemList getItems(String brand, String category) {
        List<Item> list = makeupRepository.findByBrandContainsAndCategoryContains(brand, category);
        if(!list.isEmpty()) {
            return new ItemList(list);
        }
        return null;
    }

//    public ItemList getItemsByBrand(String brand) {
//        return null;
//    }
//
//    public ItemList getItemsByCategory(String category) {
//        return null;
//    }

    public Item addItem(Item newItem) {
        Item item = makeupRepository.save(newItem);
        return item;
    }

    public Item getItem(String barcode) {
        return makeupRepository.findByBarcode(barcode).orElse(null);
    }

    public Item updateItem(String barcode, String color) {
        Optional<Item> oldItem = makeupRepository.findByBarcode(barcode);
        if(oldItem.isPresent()){
            oldItem.get().setColor(color);
            return makeupRepository.save(oldItem.get());
        }
        return null;
    }

    public void deleteItem(String barcode) {
        Optional<Item> oldItem = makeupRepository.findByBarcode(barcode);
        if(oldItem.isPresent()) {
            makeupRepository.delete(oldItem.get());
        } else {
            throw new ItemNotFoundException();
        }
    }
}
