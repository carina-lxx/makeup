package com.galvanize.makeup;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.ArrayList;
import java.util.List;

public class ItemList {
    private List<Item> items;

    public ItemList() {
        this.items = new ArrayList<>();
    }

    public ItemList(List<Item> items) {
        this.items = items;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }
    @JsonIgnore
    public boolean isEmpty() {
        return this.items.isEmpty();
    }
}
