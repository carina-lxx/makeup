package com.galvanize.makeup;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface MakeupRepository extends JpaRepository<Item, Long> {
    List<Item> findByBrandContainsAndCategoryContains(String brand, String category);
    Optional<Item> findByBarcode(String barcode);
}
