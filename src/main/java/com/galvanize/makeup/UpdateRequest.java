package com.galvanize.makeup;

public class UpdateRequest  {
    private String color;

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

}
