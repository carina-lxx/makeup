package com.galvanize.makeup;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.test.context.TestPropertySource;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static org.assertj.core.api.Assertions.assertThat;
@TestPropertySource(locations= "classpath:application-test.properties")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class MakeupApiApplicationTests {

    @Autowired
    TestRestTemplate restTemplate;

    @Autowired
    MakeupRepository makeupRepository;

    Random r = new Random();
    List<Item> testList;
    @BeforeEach
    void setUp() {
        this.testList = new ArrayList<>();
        Item item;
        String[] colors = {"cranberry", "red", "shine", "pink", "orange", "matte", "nude", "rose", "plum", "blue", "tulip", "blink pink", "rouge"};
        for (int i = 0; i < 72; i++) {
            Item auto;
            if (i % 12 == 0) {
                auto = new Item("Dior", "lipstick", "ABABAB"+(i * 13));
                auto.setColor(colors[r.nextInt(12)]);
            } else if (i % 3 == 0) {
                auto = new Item("MAC", "eyeshadow", "XYXYXY"+(i * 14));
                auto.setColor(colors[r.nextInt(12)]);
            } else {
                auto = new Item("YSL", "highlight", "OKOKOK"+(i * 15));
                auto.setColor(colors[r.nextInt(12)]);
            }
            testList.add(auto);
        }
        makeupRepository.saveAll(testList);

    }

    @AfterEach
    void tearDown() {
        makeupRepository.deleteAll();
    }

    @Test
	void contextLoads() {
	}

	@Test
    void getItems_exists_returnsList() {
        ResponseEntity<ItemList> response = restTemplate.getForEntity("/api/makeups", ItemList.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody().isEmpty()).isFalse();
    }

    @Test
    void getItems_withParams_returnList(){
        int index = r.nextInt(72);
        String category = testList.get(index).getCategory();
        String brand = testList.get(index).getBrand();
        ResponseEntity<ItemList> response = restTemplate.getForEntity(String.format("/api/makeups?brand=%s&category=%s", brand, category), ItemList.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody().isEmpty()).isFalse();
        assertThat(response.getBody().getItems().size()).isGreaterThanOrEqualTo(1);
    }

    @Test
    void addItem_returnNewItemDetail(){
        Item item = new Item("NARS", "lipstick", "QWER");
        String barcode = item.getBarcode();


        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", MediaType.APPLICATION_JSON_VALUE);
        HttpEntity<Item> request = new HttpEntity<>(item, headers);

        ResponseEntity<Item> response = restTemplate.postForEntity("/api/makeups", request, Item.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody().getBarcode()).isEqualTo(barcode);
    }


}
