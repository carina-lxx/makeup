package com.galvanize.makeup;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatExceptionOfType;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class MakeupServiceTest {

    private MakeupService makeupService;

    @Mock
    MakeupRepository makeupRepository;

    @BeforeEach
    void setUp() {
        makeupService = new MakeupService(makeupRepository);
    }

    @Test
    void getItems_noArgs_returnList() {
        Item item = new Item("Dior", "eyeshadow", "BVC");
        when(makeupRepository.findAll()).thenReturn(Arrays.asList(item));
        ItemList list = makeupService.getItems();
        assertThat(list).isNotNull();
        assertThat(list.isEmpty()).isFalse();
    }

    @Test
    void getItemds_Search() {
        Item item = new Item("Dior", "eyeshadow", "BVC");
        when(makeupRepository.findByBrandContainsAndCategoryContains(anyString(), anyString())).thenReturn(Arrays.asList(item));
        ItemList list = makeupService.getItems("Dior", "eyeshadow");
        assertThat(list).isNotNull();
        assertThat(list.isEmpty()).isFalse();
    }

    @Test
    void addItem_valid_returnsItem() {
        Item item = new Item("Dior", "eyeshadow", "BVC");

        when(makeupRepository.save(any(Item.class))).thenReturn(item);
        Item product = makeupService.addItem(item);
        assertThat(product).isNotNull();
        assertThat(product.getCategory()).isEqualTo("eyeshadow");
    }

    @Test
    void getItem_withBarcode_returnItem() {
        Item item = new Item("Dior", "eyeshadow", "BVC");
        when(makeupRepository.findByBarcode(anyString()))
                .thenReturn(java.util.Optional.of(item));
        Item product = makeupService.getItem(item.getBarcode());
        assertThat(product).isNotNull();
        assertThat(product.getBarcode()).isEqualTo("BVC");
    }

    @Test
    void updateItem_returnsItem() {
        Item item = new Item("Dior", "eyeshadow", "BVC");
        when(makeupRepository.findByBarcode(anyString()))
                .thenReturn(java.util.Optional.of(item));
        when(makeupRepository.save(any(Item.class))).thenReturn(item);
        Item product = makeupService.updateItem(item.getBarcode(), "purple");
        assertThat(product).isNotNull();
        assertThat(product.getBarcode()).isEqualTo(item.getBarcode());
    }

    @Test
    void deleteItem_byBarcode() {
        Item item = new Item("Dior", "eyeshadow", "BVC");
        when(makeupRepository.findByBarcode(anyString()))
                .thenReturn(java.util.Optional.of(item));
        makeupService.deleteItem(item.getBarcode());
        verify(makeupRepository).delete(any(Item.class));
    }
    @Test
    void deleteItem_byBarcode_notExists() {
        when(makeupRepository.findByBarcode(anyString()))
                .thenReturn(Optional.empty());
       assertThatExceptionOfType(ItemNotFoundException.class)
               .isThrownBy(() -> {
                   makeupService.deleteItem("NOT-EXISTS_BARCODE");
               });
    }

}