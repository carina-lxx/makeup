package com.galvanize.makeup;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(MakeupController.class)
public class MakeupControllerTests {
    @Autowired
    MockMvc mockMvc;

    @MockBean
    MakeupService makeupService;

    ObjectMapper mapper = new ObjectMapper();
    // GET: /api/makeups
    // GET:  /api/makeups   returns  all makeups
    @Test
    void getMakeup_noParms_exists_returnsMakeupList() throws Exception {
        List<Item> items = new ArrayList<>();
        for(int i = 0; i < 5; i++) {
            items.add(new Item("Dior", "lipstick", "ABCD"+i));
        }
        when(makeupService.getItems()).thenReturn(new ItemList(items));
        mockMvc.perform(get("/api/makeups"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.items", hasSize(5)));
    }

    // GET:  /api/makeups   returns 204 no autos found
    @Test
    void getItems_noParms_non_returnsNoContent() throws Exception {
        when(makeupService.getItems()).thenReturn(new ItemList());
        mockMvc.perform(get("/api/makeups"))
                .andExpect(status().isNoContent());
    }

    // GET:  /api/makeups?category=lipstick   returns all lipsticks from all brands
//    @Test
//    void getItems_searchWithCategoryParm_exists_returnsItemList() throws Exception {
//        List<Item> items = new ArrayList<>();
//        for(int i = 0; i < 7; i++) {
//            items.add(new Item("Dior", "lipstick", "ABCD"+i));
//        }
//        when(makeupService.getItemsByCategory(anyString())).thenReturn(new ItemList(items));
//        mockMvc.perform(get("/api/makeups?category=lipstick"))
//                .andExpect(status().isOk())
//                .andExpect(jsonPath("$.items", hasSize(7)));
//    }
//
//    // GET:  /api/makeups?brand=Dior   returns all dior
//    @Test
//    void getItems_searchWithBrandParm_exists_returnsItemList() throws Exception {
//        List<Item> items = new ArrayList<>();
//        for(int i = 0; i < 3; i++) {
//            items.add(new Item("Dior", "lipstick", "ABCD"+i));
//        }
//        when(makeupService.getItemsByBrand(anyString())).thenReturn(new ItemList(items));
//        mockMvc.perform(get("/api/makeups?brand=Dior"))
//                .andExpect(status().isOk())
//                .andExpect(jsonPath("$.items", hasSize(3)));
//    }

    // GET:  /api/makeups?category=Lipstick&brand=Dior   returns Dior lipsticks
    @Test
    void getItems_searchParms_exists_returnsItemList() throws Exception {
        List<Item> items = new ArrayList<>();
        for(int i = 0; i < 6; i++) {
            items.add(new Item("Dior", "lipstick", "ABCD"+i));
        }
        when(makeupService.getItems(anyString(), anyString())).thenReturn(new ItemList(items));
        mockMvc.perform(get("/api/makeups?brand=Dior&category=lipstick"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.items", hasSize(6)));
    }

    // POST: /api/makeups
    // /api/makeups  returns new makeup arrived store
    @Test
    void addItem_valid_returnsItem() throws Exception {
        Item item = new Item("Dior", "lipstick", "ABCD");
        when(makeupService.addItem(any(Item.class))).thenReturn(item);
        mockMvc.perform(post("/api/makeups").contentType(MediaType.APPLICATION_JSON)
                                        .content(mapper.writeValueAsString(item)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("brand").value("Dior"));
    }

    // /api/makeups  returns error message due to bad request 400
    @Test
    void addItem_throwErrorForBadRequest() throws Exception {

        when(makeupService.addItem(any(Item.class))).thenThrow(InvalidMakeupException.class);
        mockMvc.perform(post("/api/makeups").contentType(MediaType.APPLICATION_JSON)
                .content("{\"brand\":\"Dior\",\"category\":\"lipstick\",\"barcode\":\"ASD\",\"price\":\"45\""))
                .andExpect(status().isBadRequest());
    }

    // GET: /api/makeups/{barcode}
    // /api/makeups/{barcode} returns the requested makeup product
    @Test
    void getItem_withBarcode_returnsItem () throws Exception {
        Item item = new Item("Dior", "lipstick", "ABCD");
        when(makeupService.getItem(anyString())).thenReturn(item);

        mockMvc.perform(get("/api/makeups/" + item.getBarcode()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("brand").value("Dior"));
    }

    // /api/makeups/{barcode} returns NoContent 204 item not found
    @Test
    void getItem_withBarcode_noContent_whenBarcodeNotExists () throws Exception {
        when(makeupService.getItem(anyString())).thenReturn(null);

        mockMvc.perform(get("/api/makeups/ABC"))
                .andExpect(status().isNoContent());
    }

    // PATCH: /api/makeups{barcode}
    // /api/makeups{barcode} returns patched item
    @Test
    void updateItem_withBarcodeAndPrice_whenExists () throws Exception {
        Item item = new Item("Dior", "lipstick", "ABCD");
        when(makeupService.updateItem(anyString(), anyString())).thenReturn(item);

        mockMvc.perform(patch("/api/makeups/" + item.getBarcode())
                .contentType(MediaType.APPLICATION_JSON).content("{\"color\":\"pink\"}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("color").value("pink"));
    }

    // /api/makeups{barcode} returns NoContent auto not found
    @Test
    void updateItem_withBarcodeAndColor_badRequest_whenNotExists () throws Exception {
        doThrow(new ItemNotFoundException()).when(makeupService).updateItem(anyString(), anyString());
        mockMvc.perform(patch("/api/makeups/AABBCC")
                .contentType(MediaType.APPLICATION_JSON).content("{\"color\":\"pink\"}"))
                .andExpect(status().isNoContent());
    }

    // /api/makeups{barcode} returns 400 bad request (no payload, no changes, or already done)
    @Test
    void updateItem_throwErrorForBadRequest() throws Exception {

        when(makeupService.updateItem(anyString(), anyString())).thenThrow(InvalidMakeupException.class);
        mockMvc.perform(patch("/api/makeups/AABBCC").contentType(MediaType.APPLICATION_JSON)
                .content("{\"barcode\":\"ABC\",\"color\":\"orange\"}"))
                .andExpect(status().isBadRequest());
    }

    // DELETE: /api/makeups/{barcode}
    // /api/makeups/{barcode}  Returns 202, deleted request accepted
    @Test
    void deleteItem_withBarcode_returnAccepted () throws Exception {
        mockMvc.perform(delete("/api/makeups/ABCD"))
                .andExpect(status().isAccepted());
        verify(makeupService).deleteItem(anyString());
    }

    // /api/makeups/{barcode} Returns 204, item not found
    @Test
    void deleteItem_withBarcode_returnNoContent_whenNotExists () throws Exception {
        doThrow(new ItemNotFoundException()).when(makeupService).deleteItem(anyString());
        mockMvc.perform(delete("/api/makeups/AABBCC"))
                .andExpect(status().isNoContent());
    }

}
